import { Component, OnInit, VERSION, ViewChild } from '@angular/core';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { AsistebciaService } from 'src/app/_services/asistebcia.service';
import { Alert } from 'selenium-webdriver';
declare var $: any;
declare var jquery: any;
@Component({
  selector: 'app-asistencia-qr',
  templateUrl: './asistencia-qr.component.html',
  styleUrls: ['./asistencia-qr.component.css']
})
export class AsistenciaQrComponent implements OnInit {
  ngVersion = VERSION.full;

  @ViewChild('scanner')
  scanner: ZXingScannerComponent;

  hasCameras = true;
  hasPermission: boolean;
  qrResultString: string;

  availableDevices: MediaDeviceInfo[];
  selectedDevice: MediaDeviceInfo;
  constructor(private asistencia: AsistebciaService) {

  }

  ngOnInit() {
    this.camQr()

    this.apartado()

  }
  apartado() {

    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.add("active");
    document.getElementById("Configuración Empresa").classList.remove("active");
  }

  camQr() {

    this.scanner.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
      this.hasCameras = true;

      console.log('Devices: ', devices);
      this.availableDevices = devices;

      // selects the devices's back camera by default
      // for (const device of devices) {
      //     if (/back|rear|environment/gi.test(device.label)) {
      //         this.scanner.changeDevice(device);
      //         this.selectedDevice = device;
      //         break;
      //     }
      // }
    });

    this.scanner.camerasNotFound.subscribe((devices: MediaDeviceInfo[]) => {
      console.error('An error has occurred when trying to enumerate your video-stream-enabled devices.');
    });

    this.scanner.permissionResponse.subscribe((answer: boolean) => {
      this.hasPermission = answer;
    });
  }
  //toma el resultado del qr lo muestra y compara
  handleQrCodeResult(resultString: string) {
    console.log('Result: ', resultString);
    this.qrResultString = resultString;
    let id = this.qrResultString.split("||");
    console.log("id", id)
    var date = new Date();
    let x = date.toString();
    let hora = x.slice(16, -34);
    let fecha = date.toISOString();
    let fechaac = fecha.slice(0, -14);
    var guardFecha = id[0] + " " + id[1]
    if (hora >= '09:16:00'&&hora <= '17:58:00') {
      console.log("llego bien")
      if (id[0] == fechaac) {
        this.asistencia.getuser(id[2])
          .subscribe(res => {
            var nombre = res.body.nombre
            this.asistencia.marcar(id[2], id[0], id[1], "red")
              .subscribe(res => {
                console.log(res.body.status);
                if (res.body.status == 0) {
                  alert("ya marco su turno de entrada hoy");
                } else {
                  alert("Bienvenido que tengas buen turno " + nombre);
                }
              });
          });
      } else {
        alert("la fecha no es valida");
      }

    }else if (hora <= '09:01:00') {
      console.log("llego bien")
      if (id[0] == fechaac) {
        this.asistencia.getuser(id[2])
          .subscribe(res => {
            var nombre = res.body.nombre
            this.asistencia.marcar(id[2], id[0], id[1], "green")
              .subscribe(res => {
                console.log(res.body.status);
                if (res.body.status == 0) {
                  alert("ya marco su turno de entrada hoy");
                } else {
                  alert("Bienvenido que tengas buen turno " + nombre);
                }
              });
          });
      } else {
        alert("la fecha no es valida");
      }

    } else if (hora >= '17:59:00') {
      console.log("salida")
      this.asistencia.getuser(id[2])
        .subscribe(res => {
          var nombre = res.body.nombre
          this.asistencia.marcarSalida(id[2], id[1])
            .subscribe(res => {
              console.log(res);
              if (res.body.status == 0) {
                alert("ya marco su turno de salida hoy");
              } else {
                alert("Bienvenido que tengas buen turno " + nombre);
              }
            });
        });
    } else if (hora >= '09:02:00'&&hora <= '09:15:00') {
      console.log("llego tarde")
      if (id[0] == fechaac) {
        this.asistencia.getuser(id[2])
          .subscribe(res => {
            console.log(res);
            var nombre = res.body.nombre
            this.asistencia.marcar(id[2], id[0], id[1], "orange")

              .subscribe(res => {

                if (res.body.status == 0) {
                  alert("ya marco su turno de entrada hoy");
                } else {
                  alert("Bienvenido que tengas buen turno " + nombre);
                }
              });
          });
      } else {
        alert("la fecha no es valida");
      }
    }

    this.calculardiferencia(id[1], '09:01:00');




  }

  onDeviceSelectChange(selectedValue: string) {
    console.log('Selection changed: ', selectedValue);
    this.selectedDevice = this.scanner.getDeviceById(selectedValue);
  }

  calculardiferencia(h1, h2) {
    var hora1 = (h1).split(":")
    var hora2 = (h2).split(":")

    let t1 = new Date()
    let t2 = new Date();

    t1.setHours(Number(hora1[0]), Number(hora1[1]), Number(hora1[2]));
    t2.setHours(Number(hora2[0]), Number(hora2[1]), Number(hora2[2]));

    //Aquí hago la resta
    t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes(), t1.getSeconds() - t2.getSeconds());
    var tiempoLLegada = (t1.getHours() ? t1.getHours() + (t1.getHours() > 1 ? "" : "") : "00") + (t1.getMinutes() ? ":" + t1.getMinutes() + (t1.getMinutes() > 1 ? "" : "") : "") + (t1.getSeconds() ? (t1.getHours() || t1.getMinutes() ? ":" : "") + t1.getSeconds() + (t1.getSeconds() > 1 ? "" : "") : "")
    //Imprimo el resultado
    console.log(tiempoLLegada)
    console.log()
  }
}
