import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TrabajadorService } from 'src/app/_services/trabajador.service';
declare var $: any;
declare var jquery: any;
@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.css']
})

export class AsistenciaComponent implements OnInit, AfterViewInit {
  dias: any[] = [];
  dias2: any[] = [];
  statusDias: Array<StatusDiasMes>;

  fecha: any[] = [];
  dia: any[] = [];
  asistencia: any[] = []
  asiste: any[] = []
  trabajadores: any[] = [];
  indexc;

  constructor(private service: TrabajadorService) { }

  ngOnInit() {
    this.cargaT()


    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.add("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");
  }
  ngAfterViewInit() {
    // ...
    //this.ftp_login()
    
  }

 ftp_access() {

    var username = "ftp_app_efby@efby.cl";
    var password = "Lhdy=[OP4n)R";
    var server = "ftp.aconcaguaconsultores.cl";
    var local = "3000";
    if (username && password && server && local) {
            window.open(local, '_blank', 
              'toolbar=yes,location=yes,status=yes,' + 
              'scrollbars=auto,copyhistory=no,menubar=no,width=' 
              + ((screen.availWidth/2)-12) + ',height=' + 
              (screen.availHeight-124) +',left=0,top=0,resizable=yes');
            window.open("ftp://" + username + ":" + 
              password + "@" + server, '_blank', 
              'toolbar=yes,location=yes,status=yes,' + 
              'scrollbars=auto,copyhistory=no,menubar=no,width=' 
              + ((screen.availWidth/2)-12) + ',height=' 
              + (screen.availHeight-124) +',left=' + ((screen.availWidth/2)) 
              + '),top=0,resizable=yes');
            }
        else {
            alert("NO BLANK FIELDS!");
            }
    }


  cargaT() {

    this.service.asistencia()
      .subscribe(res => {
        console.log(res)
        this.trabajadores = res.body;
        this.myFuncion(this.trabajadores)

      });

  }

  myFuncion(trabajador) {
    //console.log(trabajador)
    var today = new Date();
    var month = today.getMonth();
    let numero = this.daysInMonth(month + 1, today.getFullYear())
    //console.log(numero)
    for (let i = 1; i <= numero; i++) {
      this.dias[i - 1] = i;
    }
    //se carga con el dia cargado
   /* this.dias.forEach((dia,index) => {
      var status = new StatusDiasMes()
      status.color = "white";
      status.dias = dia
      status.nombre
      this.fecha.push(status)

    })*/
    this.trabajadores.forEach((t,index) => {
      var status = new StatusDiasMes()
      var c:string[]=[]
      var n:number[]=[]
      var h:any[]=[]
      n=JSON.parse( t.f)
      c=JSON.parse( t.color)
      h=JSON.parse( t.hora)
      status.nombre=t.nombre
      
      status.id=t.idTrabajador
      for(let i=0;i<=this.dias.length-1;i++){
        this.dias2=this.dias[i]
          status.dias[i]=this.dias[i]
      
      }
      for(let i=0;i<=this.dias.length-1;i++){
        this.dias2=this.dias[i]
          status.dias[i]="N/A"
      }
      for(let i=0;i<=this.dias.length-1;i++){
          status.dias[i]=this.dias2[i]
      }

      if(n==null){return}
      if(c==null){return}
      if(h==null){return}
      status.color.length=30
      for(let i=0;i<=n.length-1;i++){
        console.log("dd",c[i])
        status.dias[n[i]["fecha"]-1]=c[i]["color"]
        console.log("status",status.dias)
      }
      //status.dias[1]="red"

      this.fecha.push(status)
    })

    
  }

  daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

   ftp_login() {
    var login = "ftp.aconcaguaconsultores.cl/";
    var clave = "Lhdy=[OP4n)R";
    var servidor = "FTP.efby.cl";
    if (login && clave && servidor) {
    var ftp_connect = "ftp.aconcaguaconsultores.cl";
    window.open(ftp_connect);
    }
    else {
    alert("No fue posible la conexion!");
    }
    }
    
}
export class StatusDiasMes {
  nombre:string;
  id:number;
  color: any[]=[];
  dias:any[]=[];
  fecha:any[]=[];

}
