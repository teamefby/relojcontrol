export interface Trabajador {
    rut:number;
    digito:string;
    nombre:string;
    apellidop:string;
    apellidom:string;
    cdc:number;
    fc_ingreso?:Date;
    sueldo?:number;
}
export interface Excepciones {
    id:number;
    excepcion:string;
    observacion:string;
    fecha:Date;

}
export interface Amonestaciones {
    idTrabajador:number;
    idCausal:number;
    carta:string;
    fecha:string;
    nombre?:string;
    apellido?:string;
    rut?:number;
    dijito?:string;
    descripcion?:string
    causales?:string;
    cargo?:string
}
export interface Empresa {
    color1?:string;
    color2?:string;
    rut?:number;
    digito?:string;
    nombre?:string;
}
export interface Vacaciones {
    fechaInicio?:Date;
    fechaFinal?:Date;
    rut?:number;
    digito?:string;
}