import { Component, OnInit } from '@angular/core';
import { TrabajadorService } from 'src/app/_services/trabajador.service';
import { CentoCostosService } from 'src/app/_services/cento-costos.service';
import { NgForm } from '@angular/forms'

declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-centro-costo',
  templateUrl: './centro-costo.component.html',
  styleUrls: ['./centro-costo.component.css']
})
export class CentroCostoComponent implements OnInit {
trabajadores:any[]=[];
tabla:any[]=[];
descripcion=null;
descripcionE=null;
encargado=null;
encargadoE=null;
idE:number=null;
idcc=null
trabajador:any[]=[]
  constructor(private service:TrabajadorService, private centro:CentoCostosService) { }

  ngOnInit() {
    this.vista();
    this.trabaja()
    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.add("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");
  }
vista(){
  this.centro.getCentroCostos()
  .subscribe(res=>{
    console.log(res);
    let c =res.body;
    this.tabla=c;
    if(this.tabla==null)return
    for(let i =0;i<=this.tabla.length-1;i++){
      this.tabla[i]['trabajadores']=JSON.parse(this.tabla[i]['trabajadores']);
      
    }
    console.log("this.tabla", this.tabla[0]["trabajadores"]);
    
    console.log("this.tabla", this.tabla);
  });
}
  
trabaja() {
  this.service.verActualizarTrabajador("todos")
    .subscribe(res => {
      console.log(res.body);
      let t = res.body;
      this.trabajador = t;
    });
}

guardar(){
console.log(this.descripcion);
this.centro.crearCentro(this.descripcion,this.encargado)
.subscribe(res=>{
  console.log(res);
  if (res.body.status==0){
    alert("error al guardar");
  }else if (res.body.status==1){
    alert("Registro exitoso");
    $('#modal_cc').modal('hide')
    this.vista();
  }
});

}

pageRefresh(id:number ,descripcion:string) {
console.log(id)
  var r = confirm("¿seguro que desea Borrar "+descripcion+" ? ");
  if (r == true) {
      alert(" borro el centro de costos "+descripcion);
      this.centro.borrarCentro(id);
      this.vista();
  } else {
      alert(" a cancelado la operacion ");
  
  }
}
editarCentro(id:number ,descripcion:string) {
  console.log(id)
  this.idcc=id
  }

editar(){
  
  this.centro.editarCentro(this.descripcionE,this.idcc,this.encargadoE)
  .subscribe((res)=>{
    console.log(res)
    alert("Edición correcta")
    this.vista();
    $('#modal').modal('hide')
  })

  
}
}
