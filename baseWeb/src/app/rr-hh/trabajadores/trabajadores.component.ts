import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { Trabajador } from '../interfaces/trabajador';
import { TrabajadorService } from '../../_services/trabajador.service';
import { CentoCostosService } from '../../_services/cento-costos.service';

declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-trabajadores',
  templateUrl: './trabajadores.component.html',
  styleUrls: ['./trabajadores.component.css']
})
export class TrabajadoresComponent implements OnInit {
  centrocostos: any[];
  trabajadores: any[] = [];

  trabajador: Trabajador = {
    rut: null,
    digito: "",
    nombre: "",
    apellidop: "",
    apellidom: "",
    cdc: null,
    fc_ingreso: null,
    sueldo: null
  }
  cargos: any[];
  cargo: string
  actrabajador: Trabajador = {
    rut: null,
    digito: "",
    nombre: "",
    apellidop: "",
    apellidom: "",
    cdc: null
  }
  idt: number;
  trabajadoresFiltro: any[] = [];
  centroo: string
  centroid: number
  
  currentProduct: string = "";
  constructor(private service: TrabajadorService, private centro: CentoCostosService) { }

  ngOnInit() {
    this.centroCotos();
    this.cargoTrabajador();

    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.add("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");

  }

  cargoTrabajador() {
    this.service.cargo()
      .subscribe(res => {
        this.cargos = res.body;

      });


  }

  guardar(forma: NgForm) {
    this.trabajador.nombre=this.trabajador.nombre.toUpperCase()
    this.trabajador.apellidop=this.trabajador.apellidop.toUpperCase()
    this.trabajador.apellidom=this.trabajador.apellidom.toUpperCase()
    
    
    this.service.insertTrabajador(this.trabajador)
      .subscribe(res => {
        console.log(res);
        if (res.body.status == 0) {
          alert("error al guardar");
        } else if (res.body.status == 1) {
          this.service.insertCargoAtrabajador(this.cargo)
            .subscribe(res => {
              console.log(res)
            });
          alert("Registro exitoso");
          this.centroCotos();
          $('#modal_trabajador').modal('hide')
        }
      });

  }
 //(blur)="blurFunction($event.target.value)" html
  borrar(id: number, nombre: string, apellido: string) {
    console.log(id)
    var r = confirm("¿seguro que desea Borrar " + nombre + " " + apellido + " ? ");
    if (r == true) {
      alert(" a borradon a " + nombre + " " + apellido);
      this.service.borrarTrabajador(id);
      this.centroCotos();
    } else {
      alert(" a cancelado la operacion ");

    }
  }


  actizarTrabajador() {
    this.actrabajador.nombre=this.actrabajador.nombre.toUpperCase()
    this.actrabajador.apellidop=this.actrabajador.apellidop.toUpperCase()
    this.actrabajador.apellidom=this.actrabajador.apellidom.toUpperCase()
    this.service.actualizarTrabajador(this.actrabajador, this.idt)
      .subscribe(res => {
        console.log(res);
        if (res.body.status == 0) {
          alert("error al guardar");
        } else if (res.body.status == 1) {
          alert("Registro exitoso");
          this.centroCotos();
          $('#modal_editar').modal('hide')
        }
      });
  }
  botonid(id, nombre, apellidop, apellidom, CentroCosto, rut, digito) {
    console.log("id", id);
    this.idt = id;
    this.actrabajador = {
      cdc: CentroCosto,
      rut: rut,
      digito: digito,
      nombre: nombre,
      apellidop: apellidop,
      apellidom: apellidom,

    };
  }

  c(value: string) {
    console.log(value);

    this.filtrandoTrabajadores(value);
  }
  rutC() {
    this.service.verificarRut(this.trabajador)
      .subscribe(res => {
        console.log(res)
        if (res.body.status == 0) {
          alert("El rut ya existe");
        }
      });
  }
  centroCotos() {
    this.centro.getCentroCostos()
      .subscribe(res => {

        let c = res.body;
        this.centrocostos = c;
        this.trabaja("todos")

      });

  }

  trabaja(id) {
    this.service.verActualizarTrabajador(id)
      .subscribe(res => {
        console.log(res.body);
        let t = res.body;
        this.trabajadores = t;
        this.trabajadoresFiltro = this.trabajadores
      });
  }



  filtrandoTrabajadores(tipo) {//
    this.centrocostos.forEach((centro, index) => {
      this.centroo = centro.descripcion
      this.centroid = centro.CentroCosto
      switch (tipo) {
        case "todos":
          this.trabajadoresFiltro = this.trabajadores
          break;
        case this.centroo:
          this.trabajadoresFiltro = this.trabajadores
          this.trabajadoresFiltro = this.trabajadoresFiltro.filter((item) => {
            return item.CentroCosto == this.centroid
          })
          break;

        default:
          break;
      }
    })

  }


}
