import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiEmprezaComponent } from './confi-empreza.component';

describe('ConfiEmprezaComponent', () => {
  let component: ConfiEmprezaComponent;
  let fixture: ComponentFixture<ConfiEmprezaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiEmprezaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiEmprezaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
