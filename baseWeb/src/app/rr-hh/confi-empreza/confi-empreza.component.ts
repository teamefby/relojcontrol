import { Component, OnInit } from '@angular/core';
import { EmpresaServiceService } from 'src/app/_services/empresa-service.service';
import { Empresa } from '../interfaces/trabajador';

@Component({
  selector: 'app-confi-empreza',
  templateUrl: './confi-empreza.component.html',
  styleUrls: ['./confi-empreza.component.css']
})
export class ConfiEmprezaComponent implements OnInit {
empresa:Empresa={

}
descripcion:string
  constructor(private eservice:EmpresaServiceService) { }

  ngOnInit() {
    this.eservice.datosEmpresa().subscribe(res=>{
      console.log(res.body.color1)
      this.empresa.nombre=res.body.nombre
      this.empresa.rut=res.body.rut
      this.empresa.digito=res.body.digito
      this.empresa.color1=res.body.color1
      this.empresa.color2=res.body.color2
    })


    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.add("active");
  }
  guardarEmpresa(){
    console.log(this.empresa)

    this.eservice.insertEmpresa(this.empresa).
    subscribe(res=>{
      console.log(res)
      if(res.body.status==false){
        alert("error")
      }else if(res.body.status==true){
        alert("registro exitoso")
        location.reload()
      }
    });
  }
  guardarCargo(){
    console.log(this.descripcion)
    this.eservice.insertCargo(this.descripcion)
    .subscribe(res=>{
      if(res.status=false){
        alert("error en el registro")
      }else if(res.status=true){
        alert("error en el registro")
      }
      console.log(res)
    })
  }
}
