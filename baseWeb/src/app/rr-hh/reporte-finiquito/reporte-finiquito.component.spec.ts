import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteFiniquitoComponent } from './reporte-finiquito.component';

describe('ReporteFiniquitoComponent', () => {
  let component: ReporteFiniquitoComponent;
  let fixture: ComponentFixture<ReporteFiniquitoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteFiniquitoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteFiniquitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
