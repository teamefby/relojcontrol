import { Component, OnInit } from '@angular/core';
import { CentoCostosService } from 'src/app/_services/cento-costos.service';
import { TrabajadorService } from 'src/app/_services/trabajador.service';

@Component({
  selector: 'app-reporte-finiquito',
  templateUrl: './reporte-finiquito.component.html',
  styleUrls: ['./reporte-finiquito.component.css']
})
export class ReporteFiniquitoComponent implements OnInit {
  centrocostos:any[];
  trabajadores:any[]=[];
  totalTrabajador:number[]=[];
  totalTabla:number=0;

  centroo:string
  centroid:number
  trabajadoresFiltro: any[] = [];
  on:boolean=true
  checked:boolean=false
  constructor(private service:TrabajadorService,private centro:CentoCostosService) { 

    
  }

  ngOnInit() {
    this.centroCotos()
    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
     document.getElementById("Recursos Humanos_mn").classList.add("active");
     document.getElementById("Inicio").classList.remove("active");
     document.getElementById("Centro De Costos").classList.remove("active")
     document.getElementById("Trabajadores").classList.remove("active");
     document.getElementById("Calculo De Vacaciones").classList.remove("active");
     document.getElementById("Indemnizacion").classList.remove("active")
     document.getElementById("Reporte De Asistencia").classList.remove("active");
     document.getElementById("Reporte De Atrasos").classList.remove("active");
     document.getElementById("Incentivos").classList.remove("active");
     document.getElementById("Amonestacion").classList.remove("active");
     document.getElementById("Reporte De Excepciones").classList.remove("active");
     document.getElementById("Reporte De Finiquitos").classList.add("active");
     document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");



  }
  trabaja(id) {
    this.service.verActualizarTrabajador(id)
      .subscribe(res => {
        let t = res.body;
        this.trabajadores = t;
        this.trabajadoresFiltro = this.trabajadores
        this.totalTabla = 0;
        if (this.trabajadoresFiltro == null) return
        for (let i = 0; i <= this.trabajadoresFiltro.length - 1; i++) {
          var v=this.trabajadoresFiltro[i]['vacaiones']-this.trabajadoresFiltro[i]['vacacionesTomadas']
          this.trabajadoresFiltro[i]['vacaiones']=v
          if(this.on==true){
            this.totalTrabajador[i] = (this.trabajadoresFiltro[i]['sueldo'] + this.trabajadoresFiltro[i]['indemnizacion']) + (this.trabajadoresFiltro[i]['vacaiones'] * (this.trabajadoresFiltro[i]['sueldo'] / 30));
            this.totalTrabajador[i] = Math.round(this.totalTrabajador[i])
            this.totalTabla += this.totalTrabajador[i]
          }else{
            this.totalTrabajador[i] = (this.trabajadoresFiltro[i]['indemnizacion']) + (this.trabajadoresFiltro[i]['vacaiones'] * (this.trabajadoresFiltro[i]['sueldo'] / 30));
            this.totalTrabajador[i] = Math.round(this.totalTrabajador[i])
            this.totalTabla += this.totalTrabajador[i]
          }

          //console.log(this.trabajadores[i]['asistencia'])

        }
        this.trabajadoresFiltro.forEach((trabajador, index) => {
          //console.log(index)
          trabajador[trabajador.length] = this.totalTrabajador[index];
        })
      });
  }
  centroCotos() {
    this.centro.getCentroCostos()
      .subscribe(res => {
        let c = res.body;
        this.centrocostos = c;
        this.trabaja("todos")
      });

  }
  c(value: string) {

    this.trabaja(value)
  }
  filtrandoTrabajadores(tipo) {//
    this.centrocostos.forEach((centro, index) => {
      this.centroo = centro.descripcion
      this.centroid = centro.CentroCosto
      switch (tipo) {
        case "todos":
          this.trabajadoresFiltro = this.trabajadores
          break;
        case this.centroo:
          this.trabajadoresFiltro = this.trabajadores
          this.trabajadoresFiltro = this.trabajadoresFiltro.filter((item) => {
            return item.CentroCosto == this.centroid
          })
          break;

        default:
          break;
      }
    })

  }


}
