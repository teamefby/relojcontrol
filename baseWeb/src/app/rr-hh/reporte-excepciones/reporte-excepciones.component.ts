import { Component, OnInit} from '@angular/core';
import { TrabajadorService } from 'src/app/_services/trabajador.service';
import { NgForm } from '@angular/forms';
import { Excepciones } from '../interfaces/trabajador';

import { NgbTypeaheadConfig } from '@ng-bootstrap/ng-bootstrap';
declare var $ :any;
declare var jquery:any;

@Component({
  selector: 'app-reporte-excepciones',
  templateUrl: './reporte-excepciones.component.html',
  styleUrls: ['./reporte-excepciones.component.css'],
})
export class ReporteExcepcionesComponent implements OnInit {
excepciones:any[]=[];
trabajador:any[]=[];
listaE:any[]=[];
formulario:Excepciones={
  id:null,
  excepcion:"",
  observacion:"",
  fecha:  new Date(),

}

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/

  constructor(private service:TrabajadorService,
    
    ) {
    }

    currentProduct: string = "";
    
  ngOnInit() {
    

    this.GetTrabajadores()

    this.getTExcepciones();
    this.getExcepciones();

    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.add("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");

  }




getTExcepciones(){
  this.service.getTrabajadores("excepcion")
  .subscribe(res=>{
    console.log(res);
    let e =JSON.parse(res.body.Excepcion);
    this.listaE=e;
  });
  
}
getExcepciones(){
  this.service.getTrabajadores("lista_excepcion")
  .subscribe(res=>{
    console.log(res);
    let e =JSON.parse(res.body.lista_excepcion);
    this.excepciones=e;
  });
}
GetTrabajadores(){
  this.service.getTrabajadores("info")
  .subscribe(res=>{
    console.log("xxx",res);
    let e =res.body;
    this.trabajador=e;
  });
}
guardar(forma:NgForm){
  console.log("hola",forma);
  console.log(this.formulario);
  this.service.inExcepcion(this.formulario);
  $('#modal_licencia').modal('hide')
  
  this.GetTrabajadores()

  this.getTExcepciones();
  this.getExcepciones();
}


}
