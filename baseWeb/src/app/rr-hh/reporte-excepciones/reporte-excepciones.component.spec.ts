import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteExcepcionesComponent } from './reporte-excepciones.component';

describe('ReporteExcepcionesComponent', () => {
  let component: ReporteExcepcionesComponent;
  let fixture: ComponentFixture<ReporteExcepcionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteExcepcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteExcepcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
