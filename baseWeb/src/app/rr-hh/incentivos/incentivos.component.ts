import { Component, OnInit } from '@angular/core';
import { IncentivosService } from 'src/app/_services/incentivos.service';
import { CentoCostosService } from 'src/app/_services/cento-costos.service';
import { TrabajadorService } from 'src/app/_services/trabajador.service';
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-incentivos',
  templateUrl: './incentivos.component.html',
  styleUrls: ['./incentivos.component.css']
})
export class IncentivosComponent implements OnInit {

centrocostos:any[];
trabajadores:any[]=[];
incentivo:any[]=[];
rut:number;
digito:string;
incentivoid:string;
descripcion:string;

trabajadoresFiltro: any[] = [];
centroo:string
centroid:number
  constructor(private incentivos:IncentivosService,
    private service:TrabajadorService,private centro:CentoCostosService
    ) { }

  ngOnInit() {
    this.centroCotos();
    this.vista()
    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.add("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");
  }
guardar(){
  this.incentivos.insertIncentivos(this.descripcion)
  .subscribe(res=>{
    if (res.body.status==0){
      alert("error al guardar");
    }else if (res.body.status==1){
      alert("Registro exitoso");
      this.centroCotos();
      $('#modal_asignar').modal('hide')
    }
  });
}
asignar(){
  this.incentivos.asignarIncentivos(this.rut,this.digito,this.incentivoid)
  .subscribe(res=>{
    console.log(res);
    if (res.body.status==0){
      alert("error al guardar");
    }else if (res.body.status==1){
      alert("Registro exitoso");
      this.centroCotos();
      $('#modal_asignar').modal('hide')
    }
  });
}

vista(){
  this.incentivos.gettIncentivos()
  .subscribe(res=>{
    this.incentivo=res.body;
  });
}
 
c(value:string){

  this.filtrandoTrabajadores(value);
  }
  
  centroCotos(){
    this.centro.getCentroCostos()
    .subscribe(res=>{
      let c =res.body;
      this.centrocostos=c;
      this.trabaja("todos")
    });
  
  }
  trabaja(id){
    this.service.verActualizarTrabajador(id)
    .subscribe(res=>{
      let t=res.body;
      this.trabajadores=t;
      this.trabajadoresFiltro=this.trabajadores
    });
  }
  
  filtrandoTrabajadores(tipo) {//
    this.centrocostos.forEach((centro, index) => {
      this.centroo = centro.descripcion
      this.centroid = centro.CentroCosto
      switch (tipo) {
        case "todos":
          this.trabajadoresFiltro = this.trabajadores
          break;
        case this.centroo:
          this.trabajadoresFiltro = this.trabajadores
          this.trabajadoresFiltro = this.trabajadoresFiltro.filter((item) => {
            return item.CentroCosto == this.centroid
          })
          break;

        default:
          break;
      }
    })

  }
}
