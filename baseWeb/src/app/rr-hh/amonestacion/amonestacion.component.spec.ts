import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmonestacionComponent } from './amonestacion.component';

describe('AmonestacionComponent', () => {
  let component: AmonestacionComponent;
  let fixture: ComponentFixture<AmonestacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmonestacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmonestacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
