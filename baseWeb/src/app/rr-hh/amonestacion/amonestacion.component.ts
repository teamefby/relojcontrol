import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';  
  
import html2canvas from 'html2canvas';  
import { Amonestaciones } from '../interfaces/trabajador';
import { formatDate } from '@angular/common';
import { AmonestacionesService } from 'src/app/_services/amonestaciones.service';

@Component({
  selector: 'app-amonestacion',
  templateUrl: './amonestacion.component.html',
  styleUrls: ['./amonestacion.component.css']
})
export class AmonestacionComponent implements OnInit {
  carta:Amonestaciones ={
    idTrabajador:null,
    idCausal:null,
    carta:"",
    rut:null,
    fecha:formatDate(new Date(), 'yyyy/MM/dd', 'en'),

}
causales:any[]=[];
  constructor(private serviceA:AmonestacionesService) { }

  ngOnInit() {
    this.serviceA.getCausales()
    .subscribe(res=>{
      console.log(res)
      this.causales=res.body;
      console.log(this.causales)
    });
    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.add("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");
  }
  

  captureScreen()  
  {  
    var data = document.getElementById('contentToConvert');  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('MYPdf.pdf'); // Generated PDF   
    });  
  }
guardar(){
this.serviceA.getinsertAmonestaciones(this.carta);
}
}
