import { Component, OnInit } from '@angular/core';
import {formatDate} from '@angular/common';
import * as moment from 'moment'; // add this 1 of 4
import { TrabajadorService } from 'src/app/_services/trabajador.service';
import { CentoCostosService } from 'src/app/_services/cento-costos.service';
import { Vacaciones } from '../interfaces/trabajador';
import { EmpresaServiceService } from 'src/app/_services/empresa-service.service';

@Component({
  selector: 'app-vacaciones',
  templateUrl: './vacaciones.component.html',
  styleUrls: ['./vacaciones.component.css']
})
export class VacacionesComponent implements OnInit {
  centrocostos: any[];
  trabajadores: any[] = [];
  t: any[];
  c: any;
  arrfechas: any[];

  trabajadoresFiltro: any[] = [];
  centroo: string
  centroid: number
  vacacio:Vacaciones={

  }
  constructor(private service: TrabajadorService, 
    private centro: CentoCostosService,
    private emservice:EmpresaServiceService) { }

  ngOnInit() {
    this.centroCotos();
    //this.vacaciones();
    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.add("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");

  }

  filtro(value: string) {
    
    this.filtrandoTrabajadores(value)
  }


  centroCotos() {
    this.centro.getCentroCostos()
      .subscribe(res => {
        if (res.body == null) return
        console.log("centroCotos", res.body[0]["descripcion"]);
        let c = res.body;
        this.centrocostos = c;
        this.trabaja("todos")
      });

  }
  trabaja(id) {
    this.service.verActualizarTrabajador(id)
      .subscribe(res => {
        console.log("datos", res.body);
        let t = res.body;
        this.trabajadores = t;
        this.trabajadoresFiltro = this.trabajadores
      });
  }
  filtrandoTrabajadores(tipo) {//
    this.centrocostos.forEach((centro, index) => {
      this.centroo = centro.descripcion
      this.centroid = centro.CentroCosto
      switch (tipo) {
        case "todos":
          this.trabajadoresFiltro = this.trabajadores
          break;
        case this.centroo:
          this.trabajadoresFiltro = this.trabajadores
          this.trabajadoresFiltro = this.trabajadoresFiltro.filter((item) => {
            console.log(item)
            return item.CentroCosto == this.centroid
          })
          break;

        default:
          break;
      }
    })
    console.log("centro de costos", this.centrocostos);

  }
  agregarVacaciones(){
    console.log(this.vacacio)
    this.emservice.vacaciones(this.vacacio)
    .subscribe(res=>{
      console.log(res)
      if(res.body.status==1){
        this.centroCotos();
        alert("las vacaciones se an registrado correctamente")
      }else{
        alert("error en el registro vea los parametros")
      }
      
    })
  }

}
