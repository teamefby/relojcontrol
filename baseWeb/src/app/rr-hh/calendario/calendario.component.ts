import { Component, OnInit } from '@angular/core';
declare var jquery: any;
declare var $: any;

@Component({
    selector: 'app-calendario',
    templateUrl: './calendario.component.html',
    styleUrls: ['./calendario.component.css']
})


export class CalendarioComponent implements OnInit {

    constructor() { }

    ngOnInit() {
        $(() => {
            var currentYear = new Date().getFullYear();
            console.log(currentYear)

            $('#calendar').calendar({
                enableContextMenu: true,
                enableRangeSelection: true,
                contextMenuItems: [
                    {
                        text: 'Modificar',
                        click: this.editEvent
                    },
                    {
                        text: 'Eliminar',
                        click: this.deleteEvent
                    }
                ],
                mouseOnDay: (e) => {
                    if (e.events.length > 0) {
                        var content = '';

                        for (var i in e.events) {
                            content += '<div class="event-tooltip-content">'
                                + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                                + '<div class="event-motivo">' + e.events[i].motivo + '</div>'
                                + '</div>';
                        }

                        $(e.element).popover({
                            trigger: 'manual',
                            container: 'body',
                            html: true,
                            content: content
                        });

                        $(e.element).popover('show');
                    }
                },
                mouseOutDay: (e) => {
                    if (e.events.length > 0) {
                        $(e.element).popover('hide');
                    }
                },
                dayContextMenu: (e) => {
                    $(e.element).popover('hide');
                },
                language: 'es',
                style: 'background',

                selectRange: (e) => {
                    this.editEvent({ startDate: e.startDate, endDate: e.endDate });
                },
                dataSource: [
                    {
                        id: 0,
                        name: 'Año Nuevo',
                        motivo: 'Feriado',
                        startDate: new Date(2019, 0, 1),
                        endDate: new Date(currentYear, 0, 1)
                    },
                    {
                        id: 1,
                        name: 'Viernes-Sábado Santo',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 3, 19),
                        endDate: new Date(currentYear, 3, 20)
                    },
                    {
                        id: 2,
                        name: 'Dia Internacional Del Trabajo',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 4, 1),
                        endDate: new Date(currentYear, 4, 1)
                    },
                    {
                        id: 3,
                        name: 'Dia De Las Glorias Navales',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 4, 21),
                        endDate: new Date(currentYear, 4, 21)
                    },
                    {
                        id: 4,
                        name: 'San Pedro-Pablo',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 5, 29),
                        endDate: new Date(currentYear, 5, 29)
                    },
                    {
                        id: 5,
                        name: 'Dia De La Virgen Del Carmen',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 6, 16),
                        endDate: new Date(currentYear, 6, 16)
                    },
                    {
                        id: 6,
                        name: 'Asunción de la Virgen',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 7, 15),
                        endDate: new Date(currentYear, 7, 15)
                    },
                    {
                        id: 7,
                        name: 'Independencia Nacional - Día de las Glorias del Ejército - Fiestas Patrias',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 8, 18),
                        endDate: new Date(currentYear, 8, 20)
                    },
                    {
                        id: 8,
                        name: 'Encuentro de Dos Mundos',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 9, 12),
                        endDate: new Date(currentYear, 9, 12)

                    },
                    {
                        id: 9,
                        name: 'Día de las Iglesias Evangélicas Protestantes',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 9, 31),
                        endDate: new Date(currentYear, 9, 31)

                    },
                    {
                        id: 10,
                        name: 'Día de Todos los Santos',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 10, 1),
                        endDate: new Date(currentYear, 10, 1)

                    },
                    {
                        id: 11,
                        name: 'Inmaculada Concepción',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 11, 8),
                        endDate: new Date(currentYear, 11, 8)

                    },
                    {
                        id: 12,
                        name: 'Navidad',
                        motivo: 'Feriado',
                        startDate: new Date(currentYear, 11, 25),
                        endDate: new Date(currentYear, 11, 25)

                    },
                ]
            });

            $('#save-event').click(() => {
                this.saveEvent();
            });
        });

    }
    editEvent(event) {
        $('#event-modal input[name="event-index"]').val(event ? event.id : '');
        $('#event-modal input[name="event-name"]').val(event ? event.name : '');
        $('#event-modal input[name="event-motivo"]').val(event ? event.motivo : '');
        $('#event-modal input[name="event-start-date"]').datetimepicker(event.startDate);
        $('#event-modal input[name="event-end-date"]').datetimepicker(event.endDate);
        $('#event-modal').modal();
    }

    deleteEvent(event) {
        var dataSource = $('#calendar').data('calendar').getDataSource();

        for (var i in dataSource) {
            if (dataSource[i].id == event.id) {
                dataSource.splice(i, 1);
                break;
            }
        }

        $('#calendar').data('calendar').setDataSource(dataSource);
    }

    saveEvent() {
        var event = {
            id: $('#event-modal input[name="event-index"]').val(),
            name: $('#event-modal input[name="event-name"]').val(),
            motivo: $('#event-modal input[name="event-motivo"]').val(),
            startDateTimePicker: $('#event-modal input[name="event-start-date"]').datetimepicker('getDate'),
            endDateTimePicker: $('#event-modal input[name="event-end-date"]').datetimepicker('getDate')
        }

        var dataSource = $('#calendar').data('calendar').getDataSource();

        if (event.id) {
            for (var i in dataSource) {
                if (dataSource[i].id == event.id) {
                    dataSource[i].name = event.name;
                    dataSource[i].motivo = event.motivo;
                    dataSource[i].startDateTimePicker = event.startDateTimePicker;
                    dataSource[i].endDateTimePicker = event.endDateTimePicker;
                }
            }
        }
        else {
            var newId = 0;
            for (var i in dataSource) {
                if (dataSource[i].id > newId) {
                    newId = dataSource[i].id;
                }
            }

            newId++;
            event.id = newId;

            dataSource.push(event);
        }

        $('#calendar').data('calendar').setDataSource(dataSource);
        $('#event-modal').modal('hide');
    }



}
