import { Component, OnInit } from '@angular/core';
import { EmpresaServiceService } from 'src/app/_services/empresa-service.service';

@Component({
  selector: 'app-index-rr-hh',
  templateUrl: './index-rr-hh.component.html',
  styleUrls: ['./index-rr-hh.component.css']
})
export class IndexRrHhComponent implements OnInit {
  users:string=null;
  image="https://cdn.photographylife.com/wp-content/uploads/2014/06/Nikon-D810-Image-Sample-1-960x641.jpg"
  Nombre_Empresa:"Pueba"
  constructor(private eservice:EmpresaServiceService) { }

  ngOnInit() {
    this.eservice.datosEmpresa().subscribe(res=>{
      this.Nombre_Empresa=res.body.nombre
    })
   document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.add("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.remove("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");
  }

}
