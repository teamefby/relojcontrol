import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexRrHhComponent } from './index-rr-hh.component';

describe('IndexRrHhComponent', () => {
  let component: IndexRrHhComponent;
  let fixture: ComponentFixture<IndexRrHhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexRrHhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexRrHhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
