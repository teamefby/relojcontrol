import { Component, OnInit } from '@angular/core';
import { CentoCostosService } from 'src/app/_services/cento-costos.service';
import { TrabajadorService } from 'src/app/_services/trabajador.service';

@Component({
  selector: 'app-reporte-atrasos',
  templateUrl: './reporte-atrasos.component.html',
  styleUrls: ['./reporte-atrasos.component.css']
})
export class ReporteAtrasosComponent implements OnInit {
  centrocostos:any[];
  trabajadores:any[]=[];

  trabajadoresFiltro: any[] = [];
  centroo:string
  centroid:number
  constructor(private service:TrabajadorService,private centro:CentoCostosService) { }

  ngOnInit() {
    this.centroCotos();
    document.getElementById("Recursos Humanos_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Recursos Humanos_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Centro De Costos").classList.remove("active")
    document.getElementById("Trabajadores").classList.remove("active");
    document.getElementById("Calculo De Vacaciones").classList.remove("active");
    document.getElementById("Indemnizacion").classList.remove("active")
    document.getElementById("Reporte De Asistencia").classList.remove("active");
    document.getElementById("Reporte De Atrasos").classList.add("active");
    document.getElementById("Incentivos").classList.remove("active");
    document.getElementById("Amonestacion").classList.remove("active");
    document.getElementById("Reporte De Excepciones").classList.remove("active");
    document.getElementById("Reporte De Finiquitos").classList.remove("active");
    document.getElementById("Asistencia QR").classList.remove("active");
    document.getElementById("Configuración Empresa").classList.remove("active");

  }
  centroCotos(){
    this.centro.getCentroCostos()
    .subscribe(res=>{
      console.log(res);
      let c =res.body;
      this.centrocostos=c;
      this.trabaja("todos")
    });
  
  }
  trabaja(id){
    this.service.verActualizarTrabajador(id)
    .subscribe(res=>{
      console.log(res.body);
      let t=res.body;
      this.trabajadores=t;
      this.trabajadoresFiltro = this.trabajadores
    });
  }
  filtro(value:string){
  
    this.filtrandoTrabajadores(value)
    }
    filtrandoTrabajadores(tipo) {//
      this.centrocostos.forEach((centro, index) => {
        this.centroo = centro.descripcion
        this.centroid = centro.CentroCosto
        switch (tipo) {
          case "todos":
            this.trabajadoresFiltro = this.trabajadores
            break;
          case this.centroo:
            this.trabajadoresFiltro = this.trabajadores
            this.trabajadoresFiltro = this.trabajadoresFiltro.filter((item) => {
              return item.CentroCosto == this.centroid
            })
            break;
  
          default:
            break;
        }
      })
  
    }
}
