export class User {
    id: number;
    username: string;
    nombre:string;
    password: string;
    firstName: string;
    lastName: string;
    imgPersonal:string;
    cargo:string;
    token?: string;
    menu:{
        nombreMenu:string, 
        groupIcon:string, 
        pages:{url:string,name:string,icon:string}[]
    }[];
}
export class Componente {
    icon:string;
    name:string;
    url:string;
}
export class Componente2 {
    icon:string;
    name:string;
    url:string;
}

