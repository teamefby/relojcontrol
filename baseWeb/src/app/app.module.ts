import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { Interceptor } from './_helpers/interceptor';
import { ErrorInterceptor } from './_helpers/error-interceptor';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexAdminComponent } from '././administracion/index-admin/index-admin.component';
import { CargosComponent } from './administracion/cargos/cargos.component';
import { FeriadosComponent } from './administracion/feriados/feriados.component';
import { OrganigramaComponent } from './administracion/organigrama/organigrama.component';
import { TurnosComponent } from './administracion/turnos/turnos.component';
import { LoginComponent } from './login/login.component';
import { CentroCostoComponent } from '././rr-hh/centro-costo/centro-costo.component';
import { IndexRrHhComponent } from './rr-hh/index-rr-hh/index-rr-hh.component';
import { AmonestacionComponent } from './rr-hh/amonestacion/amonestacion.component';
import { IncentivosComponent } from './rr-hh/incentivos/incentivos.component';
import { IndemnizacionComponent } from './rr-hh/indemnizacion/indemnizacion.component';
import { ReporteAtrasosComponent } from './rr-hh/reporte-atrasos/reporte-atrasos.component';
import { ReporteFiniquitoComponent } from './rr-hh/reporte-finiquito/reporte-finiquito.component';

import { VacacionesComponent } from './rr-hh/vacaciones/vacaciones.component';
import { ReporteExcepcionesComponent } from './rr-hh/reporte-excepciones/reporte-excepciones.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbModalModule,NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import {MomentModule} from 'angular2-moment/moment.module';
import { TrabajadoresComponent } from './rr-hh/trabajadores/trabajadores.component';
import { AsistenciaComponent} from './rr-hh/asistencia/asistencia.component';
import { CalendarioComponent } from './rr-hh/calendario/calendario.component'
import { ZXingScannerModule } from '@zxing/ngx-scanner'; 
import { AsistenciaQrComponent } from './rr-hh/asistencia-qr/asistencia-qr.component';
import { ConfiEmprezaComponent } from './rr-hh/confi-empreza/confi-empreza.component';

@NgModule({ 
  declarations: [ 
    AppComponent,
    IndexAdminComponent,
    LoginComponent,
    CentroCostoComponent,
    IndexRrHhComponent,
    AmonestacionComponent,
    AsistenciaComponent,
    IncentivosComponent,
    IndemnizacionComponent,
    ReporteAtrasosComponent,
    ReporteFiniquitoComponent,
    TrabajadoresComponent,
    VacacionesComponent,
    CargosComponent,
    FeriadosComponent,
    OrganigramaComponent,
    TurnosComponent,
    ReporteExcepcionesComponent,
    CalendarioComponent,
    AsistenciaQrComponent,
    ConfiEmprezaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    NgbModalModule,
    MomentModule,
    ZXingScannerModule,
  
    NgbModule.forRoot(),
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
      
      })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
