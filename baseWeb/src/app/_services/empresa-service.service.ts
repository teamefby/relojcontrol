import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Empresa, Vacaciones } from '../rr-hh/interfaces/trabajador';

@Injectable({
  providedIn: 'root'
})
export class EmpresaServiceService {

  constructor(private http: HttpClient) { }

  insertEmpresa(e: Empresa) {
    let body = JSON.stringify({
      "sp": "SP_UPDATE_EMPRESA",
      "parametros": [
        e.color1,
        e.color2,
        e.rut,
        e.digito,
        e.nombre
      ],
      "shema": "ClockControl"
    })
    return this.http.post<any>(`${environment.apiUrl}`, body)
  }
  datosEmpresa(){
    let body=JSON.stringify({
      "sp": "SP_DATOS_EMPRESA",
      "parametros": [

      ],
      "shema": "ClockControl"
    })
    return this.http.post<any>(`${environment.apiUrl}`, body)
  }

  insertCargo(descripcion:string){
    let body=JSON.stringify({
      "sp": "SP_INSERT_CARGOS",
      "parametros": [
        descripcion
      ],
      "shema": "ClockControl"
    })
    return this.http.post<any>(`${environment.apiUrl}`, body)
  }
  vacaciones(datos:Vacaciones){
    let body=JSON.stringify({
      "sp": "SP_AGREGAR_VACACIONES",
      "parametros": [
        datos.rut,
        datos.digito,
        datos.fechaInicio,
        datos.fechaFinal

      ],
      "shema": "ClockControl"
    })
    return this.http.post<any>(`${environment.apiUrl}`, body)
  }
}
