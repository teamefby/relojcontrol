import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AsistebciaService {

  constructor(private http: HttpClient) { }
  

  getuser(id:any){
    let body=JSON.stringify({
      "sp": "SP_QR",
      "parametros": [
          id
      ],
      "shema": "ClockControl"
    })
    return this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp',body);
  }
  marcar(id:any,fecha:any,hora:any,color:any){
    let body=JSON.stringify({
      "sp": "SP_ASISTENCIA",
      "parametros": [
          id,
          fecha,
          hora,
          color
      ],
      "shema": "ClockControl"
    })
    return this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp',body);
  }
  marcarSalida(id:any,hora:any){
    let body=JSON.stringify({
      "sp": "SP_ASISTENCIA_MARCAR_SALIDA",
      "parametros": [
          id,

          hora,

      ],
      "shema": "ClockControl"
    })
    return this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp',body);
  }
}
