import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Amonestaciones } from '../rr-hh/interfaces/trabajador';
@Injectable({
  providedIn: 'root'
})
export class AmonestacionesService {

  constructor(private http:HttpClient) { }
  getCausales(){
    let body=JSON.stringify(
      {
        "sp": "SP_AMONESTACION_CAUSALES",
        "parametros": [
          
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
  getinsertAmonestaciones(datos:Amonestaciones){
    let body=JSON.stringify(
      {
        "sp": "SP_AMONESTACION_INSERT",
        "parametros": [
          datos.rut,
          datos.dijito,
          datos.causales,
          datos.descripcion,
          datos.fecha
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body).subscribe(res=>{console.log(res)});
  }
}
