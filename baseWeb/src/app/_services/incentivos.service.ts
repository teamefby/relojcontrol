import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IncentivosService {

  constructor(private http:HttpClient) { }

  insertIncentivos(descripcion:string){
    let body=JSON.stringify(
      {
        "sp": "SP_INCENTIVOS_INSERT",
        "parametros":
       [
        descripcion
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`,body);
  }
  gettIncentivos(){
    let body=JSON.stringify(
      {
        "sp": "SP_INCENTIVOS_VISTA",
        "parametros":
       [
        
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`,body);
  }
  asignarIncentivos(rut:number,digito:string,incentivoid:string){
    let body=JSON.stringify(
      {
        "sp": "SP_ASIGNAR_INCENTIVOS",
        "parametros":
       [
        rut,
        digito,
        incentivoid
        
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`,body);
  }
}
