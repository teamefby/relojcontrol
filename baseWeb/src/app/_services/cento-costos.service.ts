import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CentoCostosService {

  constructor(private http:HttpClient) { }
  

  getCentroCostos(){
    let body=JSON.stringify(
      {
        "sp": "SP_CENTREOCOSTOVISTA",
        "parametros": [
      
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
  crearCentro(descripcion:string,encargado:string){
    let body=JSON.stringify(
      {
        "sp": "SP_INSERTCENTROCOSTO",
        "parametros": [
          descripcion,
          encargado
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }


  editarCentro(descripcion:string,id:number,encargado:string){
    let body=JSON.stringify(
      {
        "sp": "SP_CENTROCOSTOSEDIT",
        "parametros": [
          descripcion,
          id,
         encargado
        ],
        "shema": "ClockControl"
  }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
  borrarCentro(id:number){
    let body=JSON.stringify(
      {
        "sp": "SP_CENTROCOSTOBORRAR",
        "parametros": [
          id  
        ],
        "shema": "ClockControl"
  }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body)
    .subscribe(res=>{
      console.log(res);
    });
  }
}
