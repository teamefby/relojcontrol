import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import {User, Componente, Componente2} from '../_models/user'
import { error } from 'Jquery';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
rol=2;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser'),));
    this.currentUser = this.currentUserSubject.asObservable();
   }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    let body=JSON.stringify(
      {
        "sp": "SP_INICIOSESION1",
        "parametros": [
          username,
          password
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body)
        .pipe(map(user => {
          console.log(user.body);
                // login successful if there's a jwt token in the response
                var usuaio:any[]=[]
                var usuarios=JSON.parse(user.body.info)
                usuaio=usuarios
                console.log(usuaio[0]["cargo"]);
            if (user.body.status==true&&usuaio[0]["cargo"]=="prueba"||usuaio[0]["cargo"]=="RR-HH"||usuaio[0]["cargo"]=="Administracion") {
              
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user.body));
                this.currentUserSubject.next(user.body);
                this.getMenu()
            }else if (user.body.status==false||usuaio[0]["cargo"]!="prueba") return error('Usuario o contraseña incorrecta');
            return user.body;
        }));
}
getMenu(){
  let body=JSON.stringify(
    {
      "sp": "SP_MENU",
      "parametros": [
        2
      ],
      "shema": "ClockControl"
    }
  )
  return this.http.post<any>(`${environment.apiUrl}`, body);
}
ss(){
  let body=JSON.stringify(
    {

    }
  )
  return this.http.post<any>('ftp://ftp.efby.cl/', body);
}

menu1(){
 
    return this.http.get<Componente[]>('/assets/data/menu.json');
}
menu2(){
  return this.http.get<Componente[]>('/assets/data/menuAdmin.json');
}
  logout() {
      // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
      this.currentUserSubject.next(null);
  }
}
