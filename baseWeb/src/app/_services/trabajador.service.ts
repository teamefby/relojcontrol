import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Trabajador, Excepciones } from '../rr-hh/interfaces/trabajador';

@Injectable({
  providedIn: 'root'
})
export class TrabajadorService {

  constructor(private http:HttpClient) { }

  insertTrabajador(t: Trabajador) {
    let body = JSON.stringify(
      {
        "sp": "SP_INSERTTRABAJADOR",
        "parametros": [
          t.cdc,
          t.rut,
          t.digito,
          t.nombre,
          t.apellidop,
          t.apellidom,
          t.fc_ingreso,
          t.sueldo
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }

  getTrabajadores(opcion: any) {
    let body = JSON.stringify(
      {
        "sp": "SP_TRABAJADORVER",
        "parametros": [
          opcion
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }

  borrarTrabajador(id: number) {
    let body = JSON.stringify(
      {
        "sp": "SP_TRABAJADOR_BORRAR",
        "parametros": [
          id
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body)
      .subscribe(res => {
        console.log(res);
      });
  }

  actualizarTrabajador(t: Trabajador, id: number) {
    let body = JSON.stringify(
      {
        "sp": "SP_TRABAJADOR_ACTUALIZAR",
        "parametros": [
          id,
          t.cdc,
          t.rut,
          t.digito,
          t.nombre,
          t.apellidop,
          t.apellidom
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }

  verActualizarTrabajador(id: string) {
    let body = JSON.stringify(
      {
        "sp": "SP_TRABAJADOR_FILTRO",
        "parametros": [
          id
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
  inExcepcion(t: Excepciones) {
    let body = JSON.stringify(
      {
        "sp": "SP_INSERTEXCEPCIONES",
        "parametros": [
          t.id,
          t.excepcion,
          t.fecha,
          t.observacion
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body)
      .subscribe(res => { console.log(res) });
  }

  asistencia() {
    let body = JSON.stringify(
      {
        "sp": "SP_VERASISTENCIA",
        "parametros": [

        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
  cargo() {
    let body = JSON.stringify(
      {
        "sp": "SP_VER_CARGOS",
        "parametros": [

        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
  insertCargoAtrabajador(descripcion: string) {
    let body = JSON.stringify(
      {
        "sp": "SP_INSERT_CARGO_TRABAJADOR",
        "parametros": [
          descripcion
        ],
        "shema": "ClockControl"
      }
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
  verificarRut(t: Trabajador) {
    let body = JSON.stringify(
      {
        "sp": "SP_VERIFICAR_RUT",
        "parametros":
       [
       	t.rut,
       	t.digito
        ],
        "shema": "ClockControl"
      }
    
    )
    return this.http.post<any>(`${environment.apiUrl}`, body);
  }
}
