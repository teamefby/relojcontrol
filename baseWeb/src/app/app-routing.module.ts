import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards/auth.guard';
import { IndexAdminComponent } from './administracion/index-admin/index-admin.component';
import { CargosComponent } from './administracion/cargos/cargos.component';
import { FeriadosComponent } from './administracion/feriados/feriados.component';
import { OrganigramaComponent } from './administracion/organigrama/organigrama.component';
import { TurnosComponent } from './administracion/turnos/turnos.component';
import { CentroCostoComponent } from '././rr-hh/centro-costo/centro-costo.component';
import { IndexRrHhComponent } from './rr-hh/index-rr-hh/index-rr-hh.component';
import { AmonestacionComponent } from './rr-hh/amonestacion/amonestacion.component';
import { AsistenciaComponent } from './rr-hh/asistencia/asistencia.component';
import { IncentivosComponent } from './rr-hh/incentivos/incentivos.component';
import { IndemnizacionComponent } from './rr-hh/indemnizacion/indemnizacion.component';
import { ReporteAtrasosComponent } from './rr-hh/reporte-atrasos/reporte-atrasos.component';
import { ReporteFiniquitoComponent } from './rr-hh/reporte-finiquito/reporte-finiquito.component';
import { TrabajadoresComponent } from './rr-hh/trabajadores/trabajadores.component';
import { VacacionesComponent } from './rr-hh/vacaciones/vacaciones.component';
import { ReporteExcepcionesComponent } from './rr-hh/reporte-excepciones/reporte-excepciones.component';
import { CalendarioComponent } from './rr-hh/calendario/calendario.component';
import { AsistenciaQrComponent } from './rr-hh/asistencia-qr/asistencia-qr.component';
import { ConfiEmprezaComponent } from './rr-hh/confi-empreza/confi-empreza.component';


const routes: Routes = [
  {
    path: '',
    component: IndexRrHhComponent,
    canActivate: [AuthGuard],
  },
  {path:'indexrrhh',redirectTo:'/indexrrhh',pathMatch:'full'},
  {
    path: 'cargos',
    component: CargosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'empresa',
    component: ConfiEmprezaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'asistenciaqr',
    component: AsistenciaQrComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'calendario',
    component: CalendarioComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'feriados',
    component: FeriadosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'organigrama',
    component: OrganigramaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'turnos',
    component: TurnosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'costo',
    component: CentroCostoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'indexrrhh',
    component: IndexRrHhComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'amonestacion',
    component: AmonestacionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reporteAsistencia',
    component: AsistenciaComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'incentivos',
    component: IncentivosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'indemnizacion',
    component: IndemnizacionComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reporteAtrasos',
    component: ReporteAtrasosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reporteFiniquito',
    component: ReporteFiniquitoComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'trabajadores',
    component: TrabajadoresComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'vacaciones',
    component: VacacionesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reporteExcepciones',
    component: ReporteExcepcionesComponent,
    canActivate: [AuthGuard]
  },
    {
    path: 'login',
    component: LoginComponent
    },
    { path: '**', redirectTo: 'login' }
];

export const routing = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
