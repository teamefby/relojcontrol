import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cargos',
  templateUrl: './cargos.component.html',
  styleUrls: ['./cargos.component.css']
})
export class CargosComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    document.getElementById("Administración_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Administración_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Organigrama").classList.remove("active");
    document.getElementById("Definicion De Cargos").classList.add("active");
    document.getElementById("Organización De Turnos").classList.remove("active");
    document.getElementById("Feriados Del Año").classList.remove("active");
  }

}
