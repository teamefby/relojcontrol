import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feriados',
  templateUrl: './feriados.component.html',
  styleUrls: ['./feriados.component.css']
})
export class FeriadosComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    document.getElementById("Administración_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("Administración_mn").classList.add("active");
    document.getElementById("Inicio").classList.remove("active");
    document.getElementById("Organigrama").classList.remove("active");
    document.getElementById("Definicion De Cargos").classList.remove("active");
    document.getElementById("Organización De Turnos").classList.remove("active");
    document.getElementById("Feriados Del Año").classList.add("active");
    
  }
  
}
