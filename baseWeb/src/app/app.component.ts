import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User, Componente, Componente2 } from './_models/user';
import { AuthenticationService } from './_services/authentication.service';
import * as $ from 'Jquery';
import { Observable } from 'rxjs';
import { EmpresaServiceService } from './_services/empresa-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Proyecto Base';
  currentUser: User;
  componentes: Observable<Componente[]>;
  componentes2: Observable<Componente2[]>;
  img = new Image();
  page: any[] = [];
  rol = 0;
  menu1: any[] = []
  menu2: any[] = []
  nombre: any
  cargo: any
  color = "black"


  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private eservice: EmpresaServiceService

  ) {
    this.authService.currentUser.subscribe(x => {
      
      this.currentUser = x;

    });


    this.inicio()
    this.img.src = "data:image/png;base64, Li4vYXNzZXRzL2ltZy9hdmF0YXIucG5n"
    console.log(btoa("../assets/img/avatar.png"));


  }


  inicio() {
    this.eservice.datosEmpresa()
      .subscribe(res => {
        this.color = res.body.color1
      })
    this.authService.getMenu()
      .subscribe(res => {
        let menu = JSON.parse(res.body.menu);
        this.page = menu;
        this.menu1 = JSON.parse(this.page[0]["pages"])
        //localStorage.setItem('Menu', JSON.stringify(this.page));
      });
    //this.componentes=this.authService.menu1();
    //this.componentes2=this.authService.menu2();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
