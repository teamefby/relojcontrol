import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QRmodalPage } from './qrmodal.page';

describe('QRmodalPage', () => {
  let component: QRmodalPage;
  let fixture: ComponentFixture<QRmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QRmodalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QRmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
