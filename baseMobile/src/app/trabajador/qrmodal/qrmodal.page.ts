import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-qrmodal',
  templateUrl: './qrmodal.page.html',
  styleUrls: ['./qrmodal.page.scss'],
})
export class QRmodalPage implements OnInit {
  createdcode='https://www.npmjs.com/package/ngx-qrcode2';
  id:any;
  constructor(private modalctr:ModalController,    private storage: Storage,
    ) 
  {
    this.tiempo();
   }

  ngOnInit() {
    this.storage.get('id')
    .then( id=>{
      let date=new Date();
     let x=date.toString();
      let hora = x.slice(16, -34);
    
      let fecha = date.toISOString();
      let fechaac = fecha.slice(0, -14);
      alert(fechaac);
      let info=(fechaac+"||"+hora+"||"+id)
      this.createdcode = info;
    });
  }
cerrarModal(){
  this.modalctr.dismiss();
}

tiempo(){
  setTimeout(()=>{
    this.cerrarModal()
  },30000)
}
}
