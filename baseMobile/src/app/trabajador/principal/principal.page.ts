import { Component, OnInit } from '@angular/core';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { MenuController, ModalController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LoginUserService } from 'src/app/services/login-user.service';
import { Storage } from '@ionic/storage';
declare var window;
import { QRmodalPage } from '../qrmodal/qrmodal.page';
import { FTP } from '@ionic-native/ftp/ngx';
@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {
  image: string = null;
  info: any[] = [];
  unsermane: string
  cargo: string
  activoTurno = false;
  constructor(private faio: FingerprintAIO,
    private camera: Camera,
    private storage: Storage,
    private service: LoginUserService,
    private modalController: ModalController,
    private menu: MenuController,
    private fTP: FTP
  ) {

    this.menu.enable(true);

  }
  ngOnInit() {
    this.fTP.connect('Ftp://FTP.efby.cl',' ftp_app_efby@efby.cl','[OP4n)R')
      .then((res: any) => alert('Login successful'+ res))
      .catch((error: any) => alert("ddd"+error));

    this.menu.enable(true);
    //this.mostarimg();
    this.storage.get('infox')
      .then(info => {
        console.log("infox", info);
        let informa = JSON.parse(info)
        this.info = informa

      });

  }

  FingerprintAIO() {
    this.faio.show({
      clientId: 'Fingerprint-Demo',
      clientSecret: 'password', //Only necessary for Android
      disableBackup: true,  //Only for Android(optional)
      localizedFallbackTitle: 'Use Pin', //Only for iOS
      localizedReason: 'Please authenticate' //Only for iOS
    })

      .then((result: any) => console.log(result))
      .catch((error: any) => console.log(error));
  }

  camara() {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      //alert(imageData)

      this.image = (<any>window).Ionic.WebView.convertFileSrc(imageData);
      alert("convertida " + JSON.stringify(this.image))
      alert("error " + JSON.stringify(imageData))



      this.storage.get('id')
        .then(id => {
          const valido = this.service.guardar(imageData, id);
          if (valido) {
            alert("bien ")
            //this.modalctr.dismiss();
          } else {
            alert("mal ")
            // this.uiService.alertaInfo('vuelva a intentar');
          }

        }, (err) => {
          // Handle error
          alert("error " + JSON.stringify(err))
        });
    });

  }

  mostarimg() {
    this.service.mostarimagen()
      .subscribe(res => {
        this.image = (<any>window).Ionic.WebView.convertFileSrc(res.body.imagen);

      }, (err) => {
        // Handle error
        alert("error " + JSON.stringify(err))
      });
  }

  async qrModal() {

    this.activoTurno = true
    const modal = await this.modalController.create({
      component: QRmodalPage,
      mode: "ios",
      animated: true,
      //cssClass:"adf",
      // showBackdrop:true
      // componentProps: { value: 123 }
    });
    return await modal.present();
  }

}
