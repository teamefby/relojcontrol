import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { PrincipalPage } from './principal.page';
import { NgxQRCodeModule } from 'ngx-qrcode2'; 
import { QRmodalPage } from '../qrmodal/qrmodal.page';
import { QRmodalPageModule } from '../qrmodal/qrmodal.module';
const routes: Routes = [
  {
    path: '',
    component: PrincipalPage
  }
];

@NgModule({
  entryComponents:[
    QRmodalPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxQRCodeModule,
    QRmodalPageModule,
    RouterModule.forChild(routes)
  ],
  declarations:
   [
     PrincipalPage,
    
  
  ],
  providers: [

    BarcodeScanner

  ],
})
export class PrincipalPageModule {}
