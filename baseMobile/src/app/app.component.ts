import { Component } from '@angular/core';

import { Platform, Events, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';
import { Router,RouterEvent } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { LoginUserService } from './services/login-user.service';
import { Observable, Subscriber } from 'rxjs';
import { Storage } from '@ionic/storage';
declare var OurCodeWorldpreventscreenshots;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
   appPages :any[]=[];
   username:any;
  /*public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];*/
  selectedPath = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService:AuthenticationService,
    private router:Router,
    private events:Events,
    private cerrar: MenuController,
    private navctrl:NavController,
    private loginservices:LoginUserService,
    private storage:Storage
  ) {
    this.loginservices.getmenu()
    .subscribe(res=>{
      console.log(res)
      let menu1=JSON.parse(res.body.menu);
      this.appPages=menu1;

    });
    this.cerrar.enable(true);
    //para no sacar captura de pantalla
    document.addEventListener("deviceready", ()=>{
      var successCallback = ()=>{
          console.log("The screenshots are not allowed now.");
      };
  
      var errorCallback = (err)=>{
          console.error("An error ocurred : " + err);
      };
  
      OurCodeWorldpreventscreenshots.disable(successCallback,errorCallback);
  }, false);
  

   /* this.initializeApp();
    events.subscribe('user:created', (user, time) => {
      this.loginservices.getmenu()
      .subscribe(res=>{
        console.log(res)
        let menu1=JSON.parse(res.body.menu);
        this.appPages=menu1;

      });

    });*/

    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();  
     // this.appPages=this.loginservices.getmenu(1);
     this.storage.get('username')
     .then( username=>{
        this.storage.get('pass')
        .then( pass=>{
          this.loginservices.login2(username,pass)
          .subscribe(res=>{
            if(res.body.status==true){
        
              if(res.body.rol==1){
                this.createUser(res.body.rol);
                this.navctrl.navigateRoot('principalTrabajador');
                console.log(res.body.menu);
              }else if(res.body.rol==2){
                this.createUser(res.body.rol);
                this.navctrl.navigateRoot('principalEmpleador');
              }
            }else if(res.body.status==false){
             
              //this.alerta.alertas('Usuario o contraseña invalidos');
              this.navctrl.navigateRoot('login');
            }
          });
        });
     });
      

    });
  }

  setAppPages(pages){
    this.appPages=pages;
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['login'])
    this.cerrar.close();
    this.storage.clear();
  }
  createUser(user) {
    this.events.publish('user:created', user, Date.now());
  }
}
