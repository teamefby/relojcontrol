import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { ModalPageModule } from './pages/modal/modal.module';
import { Modal2PageModule } from './pages/modal2/modal2.module';
import { ModalTrabajador1PageModule } from './pages/modal-trabajador1/modal-trabajador1.module';
import { ModalIncenPageModule } from './pages/modal-incen/modal-incen.module';
import { ModalAsigPageModule } from './pages/modal-asig/modal-asig.module';
import { ModalJustiPageModule } from './pages/modal-justi/modal-justi.module';
import { ModalListPageModule } from './pages/modal-list/modal-list.module';
import { ModalPreviaPageModule } from './pages/modal-previa/modal-previa.module';
import { Camera } from '@ionic-native/camera/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FormsModule } from '@angular/forms';
import { NgxQRCodeModule } from 'ngx-qrcode2'; 
import { FTP } from '@ionic-native/ftp/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    ModalPageModule,
    Modal2PageModule,
    ModalTrabajador1PageModule,
    ModalIncenPageModule,
    ModalAsigPageModule,
    ModalJustiPageModule,
    ModalListPageModule,
    ModalPreviaPageModule,
    FormsModule,
    NgxQRCodeModule
  ],
  providers: [
    StatusBar,
    Base64ToGallery,
    SplashScreen, FingerprintAIO ,
    BarcodeScanner,
    FTP,
    Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    LocalNotifications
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
