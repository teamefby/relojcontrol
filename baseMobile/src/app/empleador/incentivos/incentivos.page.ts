import { Component, OnInit } from '@angular/core';
import { ModalController} from '@ionic/angular'
import { ModalIncenPage } from 'src/app/pages/modal-incen/modal-incen.page';
import { ModalAsigPage } from 'src/app/pages/modal-asig/modal-asig.page';
import { ModalListPage } from 'src/app/pages/modal-list/modal-list.page';

@Component({
  selector: 'app-incentivos',
  templateUrl: './incentivos.page.html',
  styleUrls: ['./incentivos.page.scss'],
})
export class IncentivosPage implements OnInit {

  constructor(public modalController : ModalController, public modalAsigController: ModalController, public modalListController: ModalController) { }

  async openModal() {
    const modal = await this.modalController.create({
      component: ModalIncenPage
    });
  modal.present();
  }

  async openModalAsig() {
    const modal = await this.modalAsigController.create({
      component: ModalAsigPage
    });
  modal.present();
  }

  async openModalList() {
    const modal = await this.modalListController.create({
      component: ModalListPage
    });
  modal.present();
  }


  ngOnInit() {
  }

}
