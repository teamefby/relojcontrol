import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalTrabajador1Page } from '../../pages/modal-trabajador1/modal-trabajador1.page';

@Component({
  selector: 'app-trabajadores',
  templateUrl: './trabajadores.page.html',
  styleUrls: ['./trabajadores.page.scss'],
})
export class TrabajadoresPage implements OnInit {

  constructor(private modalController: ModalController) {}

  async openModal() {
    const modal = await this.modalController.create({
      component: ModalTrabajador1Page
    });
  modal.present();
  }
  ngOnInit() {
  }

}
