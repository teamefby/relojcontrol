import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../../pages/modal/modal.page';
import { Modal2Page } from '../../pages/modal2/modal2.page'

@Component({
  selector: 'app-aprobacion',
  templateUrl: './aprobacion.page.html',
  styleUrls: ['./aprobacion.page.scss'],
})
export class AprobacionPage {

  constructor(public modalController: ModalController, public modal2Controller: ModalController) {}

  async openModal() {
    const modal = await this.modalController.create({
      component: ModalPage
    });
  modal.present();
  }

  async openModal2(){
    const modal = await this.modal2Controller.create({
      component: Modal2Page
    });
  modal.present();
  }


}
