import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprobacionPage } from './aprobacion.page';

describe('AprobacionPage', () => {
  let component: AprobacionPage;
  let fixture: ComponentFixture<AprobacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprobacionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprobacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
