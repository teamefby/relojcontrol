import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPreviaPage } from 'src/app/pages/modal-previa/modal-previa.page';

@Component({
  selector: 'app-amonestacion',
  templateUrl: './amonestacion.page.html',
  styleUrls: ['./amonestacion.page.scss'],
})
export class AmonestacionPage implements OnInit {

  constructor(public modalPreviaController: ModalController) { }


  async openModalPrevia() {
    const modal = await this.modalPreviaController.create({
      component: ModalPreviaPage
    });
  modal.present();
  }


  ngOnInit() {
  }

}
