import { Component, OnInit } from '@angular/core';
import { ModalController} from '@ionic/angular'
import { ModalJustiPage } from 'src/app/pages/modal-justi/modal-justi.page';

@Component({
  selector: 'app-justificacion',
  templateUrl: './justificacion.page.html',
  styleUrls: ['./justificacion.page.scss'],
})
export class JustificacionPage implements OnInit {

  constructor(public modalController : ModalController) { }

  async openModalJusti() {
    const modal = await this.modalController.create({
      component: ModalJustiPage
    });
  modal.present();
  }

  ngOnInit() {
  }

}
