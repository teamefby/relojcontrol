import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtrasosPage } from './atrasos.page';

describe('AtrasosPage', () => {
  let component: AtrasosPage;
  let fixture: ComponentFixture<AtrasosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtrasosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtrasosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
