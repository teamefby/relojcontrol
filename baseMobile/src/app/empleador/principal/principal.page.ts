import { Component, OnInit } from '@angular/core';
import { Platform, AlertController, MenuController } from '@ionic/angular';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { setBindingRoot } from '@angular/core/src/render3/state';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {
  schedule = [];

  constructor(private plt: Platform,
     private localNotifications: LocalNotifications, 
     private alertCtrl: AlertController,
     private menu:MenuController
     ) { 
    this.plt.ready().then(() => {
      this.localNotifications.on('click').subscribe(res => {
        console.log('click: ', res);
        let msg= res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg);
      })

      this.localNotifications.on('trigger').subscribe(res => {
        console.log('trigger: ', res);
        let msg= res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg);
      })
    
    });
   }

   ngOnInit() { 
    this.repeatingDaily();
    this.menu.enable(true);
   }
   repeatingDaily(){
     this.localNotifications.schedule({
       id: 1,
       title: 'Alerta de Inacistencia',
       text: 'Un trabajador tiene bajo % de asistencia',
       data: { page: '/reporteAsistencia.page.html'},
       trigger: { in: 5, unit: ELocalNotificationTriggerUnit.SECOND},
      
     })
   }

   showAlert(header, sub, msg){
     this.alertCtrl.create({
       header: header,
       subHeader: sub,
       message: msg,
       buttons: ['OK']
     }).then(alert => alert.present());
   }

}
