import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-modal-previa',
  templateUrl: './modal-previa.page.html',
  styleUrls: ['./modal-previa.page.scss'],
})
export class ModalPreviaPage implements OnInit {

  constructor(public modalPreviaController: ModalController) { }

  ngOnInit() {
  }


  dismiss(){
    this.modalPreviaController.dismiss(); 
  }

}
