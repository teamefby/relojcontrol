import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPreviaPage } from './modal-previa.page';

describe('ModalPreviaPage', () => {
  let component: ModalPreviaPage;
  let fixture: ComponentFixture<ModalPreviaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPreviaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPreviaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
