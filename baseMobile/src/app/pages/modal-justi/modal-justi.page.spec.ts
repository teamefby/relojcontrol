import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalJustiPage } from './modal-justi.page';

describe('ModalJustiPage', () => {
  let component: ModalJustiPage;
  let fixture: ComponentFixture<ModalJustiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalJustiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalJustiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
