import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-modal-justi',
  templateUrl: './modal-justi.page.html',
  styleUrls: ['./modal-justi.page.scss'],
})
export class ModalJustiPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }


  dismiss(){
    this.modalController.dismiss(); 
  }

}
