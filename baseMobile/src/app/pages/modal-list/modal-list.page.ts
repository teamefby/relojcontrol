import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-modal-list',
  templateUrl: './modal-list.page.html',
  styleUrls: ['./modal-list.page.scss'],
})
export class ModalListPage implements OnInit {

  constructor(public modalListController: ModalController) { }

  ngOnInit() {
  }


  dismiss(){
    this.modalListController.dismiss(); 
  }

}
