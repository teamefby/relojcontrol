import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-modal-incen',
  templateUrl: './modal-incen.page.html',
  styleUrls: ['./modal-incen.page.scss'],
})
export class ModalIncenPage implements OnInit {

   constructor(public modalController: ModalController) { }

  ngOnInit() {
  }


  dismiss(){
    this.modalController.dismiss(); 
  }

}
