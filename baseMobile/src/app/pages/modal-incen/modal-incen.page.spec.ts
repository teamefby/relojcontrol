import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalIncenPage } from './modal-incen.page';

describe('ModalIncenPage', () => {
  let component: ModalIncenPage;
  let fixture: ComponentFixture<ModalIncenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalIncenPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalIncenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
