import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-modal-trabajador1',
  templateUrl: './modal-trabajador1.page.html',
  styleUrls: ['./modal-trabajador1.page.scss'],
})
export class ModalTrabajador1Page implements OnInit {

  constructor( public modalController: ModalController) { }

  ngOnInit() {
  }

  dismiss(){
    this.modalController.dismiss(); 
  }
}
