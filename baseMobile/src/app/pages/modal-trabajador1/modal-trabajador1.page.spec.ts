import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTrabajador1Page } from './modal-trabajador1.page';

describe('ModalTrabajador1Page', () => {
  let component: ModalTrabajador1Page;
  let fixture: ComponentFixture<ModalTrabajador1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTrabajador1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTrabajador1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
