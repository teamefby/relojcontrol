import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAsigPage } from './modal-asig.page';

describe('ModalAsigPage', () => {
  let component: ModalAsigPage;
  let fixture: ComponentFixture<ModalAsigPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAsigPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAsigPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
