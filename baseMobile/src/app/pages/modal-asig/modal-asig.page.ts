import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-modal-asig',
  templateUrl: './modal-asig.page.html',
  styleUrls: ['./modal-asig.page.scss'],
})
export class ModalAsigPage implements OnInit {

  constructor( public modalAsigController: ModalController) { }

  ngOnInit() {
  }

  dismiss(){
    this.modalAsigController.dismiss(); 
  }
}
