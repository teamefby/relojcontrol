import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertasService {

  constructor(public alerta:AlertController) { }
  async alertas(message:string){
    const aler=await this.alerta.create({
      message,
      buttons:['ok']
    })
    await aler.present();
  }

}
