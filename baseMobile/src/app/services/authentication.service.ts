import { Injectable } from '@angular/core';
import { resolve } from 'url';
import { reject } from 'q';
import { BehaviorSubject } from 'rxjs';

export interface User{
  name : string;
  role : number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private Empleador = {name:"empleador",pass:"1",role:0};
  private Trabajador = {name:"trabajador",pass:"1",role:1};

  currentUser : User;
  authenticationState = new BehaviorSubject(false);
  Pages=[];

  constructor() { }

  login(userName:string,passWord:string) : Promise<boolean>{
    return new Promise((resolve,reject) => {
      if(userName===this.Empleador.name && passWord===this.Empleador.pass){
        this.currentUser={
          name:this.Empleador.name,
          role:this.Empleador.role
        }
        this.Pages=[
          {title:"Principal",url:"/principalEmpleador",icon:"journal"},
          {title:"Centro de Costos",url:"/centroCosto",icon:"cash"},
          {title:"Listado de Trabajadores",url:"/trabajadores",icon:"contacts"},
          {title:"Cálculo Vacaciones Días",url:"/vacacionesDia",icon:"calendar"},
          {title:"Cálculo Vacaciones Pesos",url:"/vacacionesPesos",icon:"filing"},
          {title:"Provisión Indemnización",url:"/indemnizacion",icon:"card"},
          {title:"Reporte Asistencia Mensual",url:"/reporteAsistencia",icon:"clock"},
          //reporte atrasos
          //reporte finiquito
          {title:"Incentivos",url:"/incentivos",icon:"gift"},
          {title:"Carta Amonestación",url:"/amonestacion",icon:"mail"},
          {title:"Registro Justificación",url:"/justificacion",icon:"copy"},
          {title:"Aprobacion de Solicitudes/Consultas",url:"/aprobacion",icon:"chatboxes"}
        ];
        this.authenticationState.next(true);
        resolve(true);
      }else if (userName===this.Trabajador.name && passWord===this.Trabajador.pass) {
        this.currentUser={
          name:this.Trabajador.name,
          role:this.Trabajador.role
        }
        this.Pages=[
          {title:"Principal",url:"/principalTrabajador",icon:"journal"},
          {title:"Horario Trabajador",url:"/horario",icon:"alarm"},
          {title:"Reporte Asistencia",url:"/asistencia",icon:"book"},
          {title:"Historico Asistencias",url:"/historicoAsistencia",icon:"bookmarks"},
          {title:"Solicitudes y Mensajeria",url:"/mensajeria",icon:"chatboxes"},
          {title:"Ayuda",url:"/ayuda",icon:"help"}
        ];
        this.authenticationState.next(true);
        resolve(true);
      }else{
        this.authenticationState.next(false);
        reject(false);
      }
    });
  }

  isLoggedIn(){
    return this.authenticationState.value;
  }

  logout(){
    this.authenticationState.next(true);
    this.currentUser = null;
  }

  isEmpleador(){
    return this.currentUser.role === this.Empleador.role;
  }
  
  getMenuItems(){
    return this.Pages;
  }
}
