import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController, Events } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AlertasService } from './alertas.service';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LoginUserService {
username:string
  constructor(private http: HttpClient,
    private navctrl: NavController,
    private events: Events,
    private store: Storage,
    private alerta: AlertasService
  ) { }
  login(user: string, pass: string) {
    let body = JSON.stringify({
      "sp": "SP_INICIOSESION1",
      "parametros": [
        user,
        pass

      ],
      "shema": "ClockControl"
    });
    // return new Promise(resolve=>{
    return this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp', body)
      .subscribe(res => {
        console.log(res.body.cargo);
        if (res.body.status == true) {

          if (res.body.cargo != "Administracion" && res.body.cargo != "prueba" && res.body.cargo != "RR-HH") {
            this.navctrl.navigateRoot('principalTrabajador');
            this.createUser(res.body);
            this.guardarInfo(res.body.info,res.body.id);
            console.log(res.body);
            console.log(res.body.info);
          } else if (res.body.rol == 2) {
            // this.navctrl.navigateRoot('principalEmpleador');
            //this.createUser(res.body.rol);
            this.alerta.alertas('Usuario o contraseña invalidos');
          } else {
            this.alerta.alertas('Usuario o contraseña invalidos');
          }
        } else if (res.body.status == false) {

          this.alerta.alertas('Usuario o contraseña invalidos');
          //this.navctrl.navigateRoot('login');
        }

      });
    // });
  }
  login2(user: string, pass: string) {
    let body = JSON.stringify({
      "sp": "SP_INICIOSESION1",
      "parametros": [
        user,
        pass

      ],
      "shema": "ClockControl"
    });
    // return new Promise(resolve=>{´´``
    return this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp', body);
    //});
  }

  async guardarInfo(info:any,id:any) {
    await this.store.set('infox', info);
    await this.store.set('id', id);
  }

  getmenu() {
    let body = JSON.stringify({
      "sp": "SP_MENU_MOBILE",
      "parametros": [
        1
      ],
      "shema": "ClockControl"
    });
    return this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp', body);

  }
  createUser(user) {
    this.events.publish('user:created', user, Date.now());
  }


  guardar(image: any, id: number) {
    let body = JSON.stringify({
      "sp": "SP_IMAGETRABAJADOR",
      "parametros": [
        image,
        id
      ],
      "shema": "ClockControl"
    });
    return new Promise(resolve => {
      this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp', body)
        .subscribe(res => {
          console.log(res);
          if (res.body.status == 1) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
    });
  }
  mostarimagen() {
    let body = JSON.stringify({
      "sp": "SP_MOSTRARIMAGEN",
      "parametros": [
        1
      ],
      "shema": "ClockControl"
    });
    return this.http.post<any>('https://l7bcvmebte.execute-api.us-west-2.amazonaws.com/QA/llamarsp', body);
  }
}
