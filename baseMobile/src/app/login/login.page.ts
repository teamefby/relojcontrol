import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { AlertController, MenuController, Events } from '@ionic/angular';
import { LoginUserService } from '../services/login-user.service';
import { AlertasService } from '../services/alertas.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private authService:AuthenticationService,
    private router: Router, 
    private alertCtrl:AlertController,
    private menu:MenuController,
    private events:Events ,
    private loginUserservice:LoginUserService,
    private alerta:AlertasService
    ) { }

  ngOnInit() {
    this.menu.enable(false);
  }

  async invalidLoginAlert(){
    const alert = await this.alertCtrl.create({
      header: 'Inicio de Sesion no Valido',
      message: 'Nombre de Usuario o Contraseña Incorrecto',
      buttons: ['OK']
    });
    return await alert.present();
  }


  loginUser(user:string,pass:string){
    this.authService.login(user,pass).then(succes=>{
      if(succes){
        
        //this.createUser(this.authService.currentUser);
        if(this.authService.isEmpleador()){
          this.router.navigate(['principalEmpleador']);
        }else{
          this.router.navigate(['principalTrabajador']);
        }
      }
    }).catch(err => {
      this.invalidLoginAlert();
    });
  }


  login(user:string,pass:string){
    const valido= this.loginUserservice.login(user,pass);
    if( valido ){
        
        
    }else{
      this.alerta.alertas('Usuario o contraseña invalidos');
    }

  }




}
