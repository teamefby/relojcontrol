import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'principalTrabajador', loadChildren: './trabajador/principal/principal.module#PrincipalPageModule' },
  { path: 'horario', loadChildren: './trabajador/horario/horario.module#HorarioPageModule' },
  { path: 'ayuda', loadChildren: './trabajador/ayuda/ayuda.module#AyudaPageModule' },
  { path: 'mensajeria', loadChildren: './trabajador/mensajeria/mensajeria.module#MensajeriaPageModule' },
  { path: 'asistencia', loadChildren: './trabajador/asistencia/asistencia.module#AsistenciaPageModule' },
  { path: 'historicoAsistencia', loadChildren: './trabajador/historico-asistencia/historico-asistencia.module#HistoricoAsistenciaPageModule' },
  { path: 'principalEmpleador', loadChildren: './empleador/principal/principal.module#PrincipalPageModule' },
  { path: 'centroCosto', loadChildren: './empleador/centro-costo/centro-costo.module#CentroCostoPageModule' },
  { path: 'trabajadores', loadChildren: './empleador/trabajadores/trabajadores.module#TrabajadoresPageModule' },
  { path: 'vacacionesDia', loadChildren: './empleador/vacaciones-dia/vacaciones-dia.module#VacacionesDiaPageModule' },
  { path: 'vacacionesPesos', loadChildren: './empleador/vacaciones-pesos/vacaciones-pesos.module#VacacionesPesosPageModule' },
  { path: 'indemnizacion', loadChildren: './empleador/indemnizacion/indemnizacion.module#IndemnizacionPageModule' },
  { path: 'reporteAsistencia', loadChildren: './empleador/reporte-asistencia/reporte-asistencia.module#ReporteAsistenciaPageModule' },
  { path: 'incentivos', loadChildren: './empleador/incentivos/incentivos.module#IncentivosPageModule' },
  { path: 'amonestacion', loadChildren: './empleador/amonestacion/amonestacion.module#AmonestacionPageModule' },
  { path: 'justificacion', loadChildren: './empleador/justificacion/justificacion.module#JustificacionPageModule' },
  { path: 'aprobacion', loadChildren: './empleador/aprobacion/aprobacion.module#AprobacionPageModule' },
  { path: 'atrasos', loadChildren: './empleador/atrasos/atrasos.module#AtrasosPageModule' },
  { path: 'modal', loadChildren: './pages/modal/modal.module#ModalPageModule' },
  { path: 'modal2', loadChildren: './pages/modal2/modal2.module#Modal2PageModule' },
  { path: 'modal-trabajador1', loadChildren: './pages/modal-trabajador1/modal-trabajador1.module#ModalTrabajador1PageModule' },
  { path: 'modal-incen', loadChildren: './pages/modal-incen/modal-incen.module#ModalIncenPageModule' },
  { path: 'modal-asig', loadChildren: './pages/modal-asig/modal-asig.module#ModalAsigPageModule' },
  { path: 'modal-justi', loadChildren: './pages/modal-justi/modal-justi.module#ModalJustiPageModule' },
  { path: 'modal-list', loadChildren: './pages/modal-list/modal-list.module#ModalListPageModule' },
  { path: 'modal-previa', loadChildren: './pages/modal-previa/modal-previa.module#ModalPreviaPageModule' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
